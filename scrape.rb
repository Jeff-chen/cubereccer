require 'nokogiri'

require 'open-uri'

indexes = 1.upto(9554)

my_hash = {}

hash_of_cubes = {}

indexes.each do |ii|
  hash_of_cubes[ii] = []
  puts ii
  #TODO: this can be a 500 error; if so the script stops. id 9555 will throw an error lol
  page = Nokogiri::HTML(open("http://www.cubetutor.com/viewcube/#{ii}"))

  cube_cards = page.css('a.cardPreview')

  cards = cube_cards.map{|i| i.children.text}
  if cards.size > 100
    output_z = File.open("lists/cube#{ii}.txt", "w")
    cards.each do |ele|
      output_z << ele + "\n"
    end
    output_z.close
  end
  #hash_of_cubes[ii] << cards
  cards.each{|card| my_hash[card] ? my_hash[card] += 1 : my_hash[card] = 1}
  puts "----"
end

puts my_hash.inspect

#hash_of_cubes.each do |index, vals|
#  puts index
#  if vals.first.size > 100
#    output_z = File.open("lists/cube#{index}.txt", "w")
#    vals.first.each do |ele|
#      output_z << ele + "\n"
#    end
#    output_z.close
#  end
#  
#end

output_data = File.open("sayhi4.txt", "w")
my_hash.sort.sort{|a,b| b[1] <=> a[1]}.each do |name, ct|
  output_data << "#{name},#{ct}\n"
end