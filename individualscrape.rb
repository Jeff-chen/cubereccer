require 'nokogiri'

require 'open-uri'

index = 10635

my_hash = {}

hash_of_cubes = {}

hash_of_cubes[index] = []
page = Nokogiri::HTML(open("http://www.cubetutor.com/viewcube/#{index}"))

tepigs = page.css('a.cardPreview')

cards = tepigs.map{|i| i.children.text}
hash_of_cubes[index] << cards
cards.each{|card| my_hash[card] ? my_hash[card] += 1 : my_hash[card] = 1}
puts "----"

puts my_hash.inspect

hash_of_cubes.each do |index, vals|
  puts index
  if vals.first.size > 100
    output_z = File.open("lists/cube#{index}.txt", "w")
    vals.first.each do |ele|
      output_z << ele + "\n"
    end
    output_z.close
  end
  
end

output_data = File.open("ruff.txt", "w")
my_hash.sort.sort{|a,b| b[1] <=> a[1]}.each do |name, ct|
  output_data << "#{name},#{ct}\n"
end