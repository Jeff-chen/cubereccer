hash_of_cubes = {}
list_of_cards = {}

Dir.glob('lists/cube*.txt') do |file|
  cube_id = file.gsub(/\D+/, '').to_i
  blahblah = File.open(file, 'r')
  hash_of_cubes[cube_id] = {}
  
  blahblah.readlines.each do |arglbargl|
    arglbargl.strip!
    hash_of_cubes[cube_id][arglbargl] = 1
    list_of_cards[arglbargl] = 1
  end
  
  #puts blahblah.readlines.first
  #puts file
end
DELIMITER = "\t"
summary_file = File.open("summary.tsv", "w")
summary_file << "id#{DELIMITER}"
puts list_of_cards.sort.map(&:first).inspect

mapper = File.open("cardindex.tsv","w")
list_of_cards.each_with_index do |card, i|
  mapper << "#{i+1}#{DELIMITER}#{card.first}\n"
  summary_file << "#{card.first.downcase.gsub('\'','.')}#{DELIMITER}"
end
summary_file << "\n"

puts hash_of_cubes.size
tempindex = 0
hash_of_cubes.each do |ii, cube|
  summary_file << "#{ii}#{DELIMITER}"
  list_of_cards.each do |card|
    summary_file << (cube[card.first] ? "1" : "0") + "#{DELIMITER}"
  end
  summary_file << "\n"
end