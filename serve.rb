require 'sinatra'
require 'haml'
require 'nokogiri'
require 'open-uri'

RECS_PER_CUBE = 100

class Recommendation
  attr_accessor :card_name, :rec_score
  
  def initialize(card_name, rec_score)
    @card_name = card_name
    @rec_score = rec_score
  end
end

def dotprod(v1, v2)
  sum, i, size = 0, 0, v1.size
  while i < size
    sum += v1[i] * v2[i]
    i += 1
  end
  sum
end

configure do
  card_scores_file = File.open("cardScores.csv",'r') #this file shows the top 30/100 card similarities scores for each card (row)
  card_ids_file = File.open("cardIDs.csv",'r') #this file shows the ids of the top 30 cards for each card
  cardindexfile = File.open("cardindex.tsv",'r')
  my_dict = {}
  cardindexfile.readlines.each do |l|
    k, v = l.split("\t")
    my_dict[k.to_i] = v.strip
  end
  
  s1 = []
  card_scores_file.readlines[1..-1].each do |l|
    s1 << l.strip.split(",").map(&:to_f)[1..-1]
  end
  
  s2 = []
  card_ids_file.readlines[1..-1].each do |l|
    s2 << l.strip.split(",")[1..-1].map(&:to_i)
  end
  set :my_config_property, 'hello world'
  
  set :card_index, my_dict
  set :card_scores, s1
  set :card_ids, s2
  
  card_scores_file.close
  card_ids_file.close
  cardindexfile.close
end

get '/' do
  haml :index
end

post '/recommend' do
  if params[:cubetutorid] && params[:cubetutorid] != ""
    if params[:cubetutorid] =~ /^[0-9]+$/ #only integers
      my_url = "http://www.cubetutor.com/viewcube/#{params[:cubetutorid]}"
    else
      my_url = params[:cubetutorid]
    end
    puts my_url.inspect
    page = Nokogiri::HTML(open(my_url))

    cube_card_names = page.css('a.cardPreview')

    cards = cube_card_names.map{|i| i.children.text}
    if cards.size > 40
      cube_card_ids = cards.map{|i| settings.card_index.key(i)}
    else
      cube_card_ids = nil
      #haml :recommend, :locals => {:recommendations => "This cube don't exist"}
      #return
    end
  else
    if params[:textfile]
      #raise params.inspect#[:textfile].class.inspect
      inputted_file = File.open(params[:textfile][:tempfile], 'r')
      cube_card_ids = inputted_file.readlines.map{|i| settings.card_index.key(i.strip)}
      puts cube_card_ids.inspect
      #raise 'lol'
    end
  end
  scores = []
  if cube_card_ids
    0.upto(settings.card_ids.size-1) do |i|
      match_ids = settings.card_ids[i].map do |j|
        if cube_card_ids.include? j
          1
        else
          0
        end
      end
      lolsum = settings.card_scores[i].inject{|a, b| a+b}
      if lolsum == 0.0 or cube_card_ids.include?(i)
        x = 0
      else
        x = dotprod(settings.card_scores[i], match_ids ) / settings.card_scores[i].inject{|a, b| a+b}
      end
      scores << x
    end
    cutoff = scores.sort.reverse[RECS_PER_CUBE-1]
    fun_indexes = scores.each_index.select{|i| scores[i] >= cutoff}
    recs = fun_indexes.map do |i|
      Recommendation.new(settings.card_index[i].gsub("'","\'"), scores[i])
    end
    fun_scores = scores.values_at(*fun_indexes).inspect
    
    puts fun_indexes.inspect
    
    @recommendations = recs.sort{|a,b| b.rec_score <=> a.rec_score}#fun_indexes.map{|i| settings.card_index[i].gsub("'","\'")}
    @recommendations = @recommendations.map{|i| puts i;"<img src=\"http://gatherer.wizards.com/Handlers/Image.ashx?size=small&type=card&name=#{i.card_name}&options=\">"}.join("")
    haml :recommend, :locals => {:recommendations => @recommendations}
  else
    haml :recommend, :locals => {:recommendations => "This cube doesn't exist"}
  end
end

